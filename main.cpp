#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickWindow>
#include <LayerShellQt/Shell>
#include <LayerShellQt/Window>
#include <QQmlContext>

#include "keyboardsimulator.h"

int main(int argc, char *argv[])
{
    LayerShellQt::Shell::useLayerShell();
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    KeyboardSimulator *keyboardSimulator = new KeyboardSimulator(&app);
    engine.rootContext()->setContextProperty("keyboardSimulator", keyboardSimulator);

    const QUrl url(u"qrc:/VirtKBD/Main.qml"_qs);
    QObject::connect(
        &engine,
        &QQmlApplicationEngine::objectCreationFailed,
        &app,
        []() { QCoreApplication::exit(-1); },
        Qt::QueuedConnection);
    engine.load(url);

    // Set layer shell properties
    QQuickWindow* window = (QQuickWindow*) engine.rootObjects()[0];
    LayerShellQt::Window* lwindow = LayerShellQt::Window::get(window);
    lwindow->setCloseOnDismissed(true);
    lwindow->setLayer(lwindow->LayerOverlay);
    lwindow->setKeyboardInteractivity(lwindow->KeyboardInteractivityNone);

    // Resize keyboard
    lwindow->setMargins(QMargins(0, window->screen()->availableGeometry().height() / 2, 0, 0));

    return app.exec();
}
