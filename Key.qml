import QtQuick
import QtQuick.Layouts

Rectangle {
    id: root;
    Layout.fillHeight: true;
    Layout.preferredWidth: height * 0.7 * multiplier + 12 * (multiplier - 1);
    radius: 12;
    state: "default";
    clip: true;

    property string keyName: "";
    property real multiplier: 1;
    property var onClick: null;

    Text {
        text: keyName;
        anchors.centerIn: parent;
        font.pointSize: parent.height * 0.4;
        font.family: "Alegreya Sans";
        color: "#eaf0d8";
    }

    MouseArea {
        anchors.fill: parent;

        onPressed: root.state = "pressed";
        onReleased: {
            root.state = "default";
            if (keyName == "A") keyboardSimulator.writeChar(keyName);
            if (onClick) onClick();
        }
    }

    states: [
        State {
            name: "default"
            PropertyChanges { target: root; color: "#413a42" }
        },
        State {
            name: "pressed"
            PropertyChanges { target: root; color: "#596070" }
        }
    ]

    transitions: [
        Transition {
            from: "pressed"
            to: "default"

            ColorAnimation {
                duration: 150
            }
        }
    ]
}
