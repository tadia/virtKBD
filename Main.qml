import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

Window {
    id: keyboard;
    visible: true;
    title: qsTr("Virtuální klávesnice");
    color: "#1f1f29";

    ColumnLayout {
        anchors.fill: parent;
        anchors.margins: 12;
        spacing: 12;

        KeyRow {
            Spacer {
            }

            Key {
                keyName: "Q"
            }

            Key {
                keyName: "W"
            }

            Key {
                keyName: "E"
            }

            Key {
                keyName: "R"
            }

            Key {
                keyName: "T"
            }

            Key {
                keyName: "Z"
            }

            Key {
                keyName: "U"
            }

            Key {
                keyName: "I"
            }

            Key {
                keyName: "O"
            }

            Key {
                keyName: "P"
            }
        }
        KeyRow {
            Spacer {
                multiplier: 1.5
            }

            Key {
                keyName: "A";
            }

            Key {
                keyName: "S"
            }

            Key {
                keyName: "D"
            }

            Key {
                keyName: "F"
            }

            Key {
                keyName: "G"
            }

            Key {
                keyName: "H"
            }

            Key {
                keyName: "J"
            }

            Key {
                keyName: "K"
            }

            Key {
                keyName: "L"
            }

            Spacer {
                multiplier: 0.5
            }
        }
        KeyRow {
            Spacer {
            }

            Key {
                keyName: "";
                multiplier: 1.5;
            }

            Key {
                keyName: "Y";
            }

            Key {
                keyName: "X"
            }

            Key {
                keyName: "C"
            }

            Key {
                keyName: "V"
            }

            Key {
                keyName: "B"
            }

            Key {
                keyName: "N"
            }

            Key {
                keyName: "M"
            }

            Key {
                keyName: "";
                multiplier: 1.5;
            }
        }
        KeyRow {
            Key {
                onClick: function() {
                    keyboard.close();
                }
            }

            Key {
                multiplier: 1.5;
            }

            Key {
                keyName: ","
            }

            Key {
                multiplier: 5;
            }

            Key {
                keyName: "."
            }

            ReturnKey {
                multiplier: 1.5;
            }
        }
    }
}
