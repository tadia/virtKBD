import QtQuick
import QtQuick.Layouts

Rectangle {
    id: root;
    Layout.fillHeight: true;
    Layout.preferredWidth: height * 0.7 * multiplier + 12 * (multiplier - 1);
    color: "transparent";

    property real multiplier: 1;
}
