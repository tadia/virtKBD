import QtQuick
import QtQuick.Layouts

RowLayout {
    Layout.fillHeight: true;
    Layout.alignment: Qt.AlignHCenter;
    spacing: 12;
}
