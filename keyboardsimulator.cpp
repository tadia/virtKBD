#include <QDebug>
#include <QDBusInterface>
#include <QDBusReply>
#include <QRandomGenerator>
#include "keyboardsimulator.h"

KeyboardSimulator::KeyboardSimulator(QObject *parent) {
    //requestInterface = new QDBusInterface("org.freedesktop.portal.Desktop", "/org/freedesktop/portal/desktop", "org.freedesktop.portal.Request", sessionBus, this);
    interface = new QDBusInterface("org.freedesktop.portal.Desktop", "/org/freedesktop/portal/desktop", "org.freedesktop.portal.RemoteDesktop", sessionBus, this);
    sessionBus.connect("", sessionReply.value().path(), "org.freedesktop.portal.Request", "Response", this, SLOT(handleResponse(uint,QVariantMap)));
}

KeyboardSimulator::~KeyboardSimulator() {
    delete interface;
}

void KeyboardSimulator::makeRequest() {
    QVariantMap sessionOptions;
    QString token = QString("tadia_") + QString::number( QRandomGenerator::global()->generate() );
    sessionOptions["session_handle_token"] = token;
    sessionReply = interface->call("CreateSession", sessionOptions);
}

void KeyboardSimulator::handleResponse(uint response, QVariantMap data) {
    switch (expectation) {
        case 0:
            {
                QString sessionPath = data["session_handle"].toString();
                QDBusObjectPath* sessionHandle = new QDBusObjectPath(sessionPath);

                QVariantMap startOptions;
                interface->call("Start", *sessionHandle, "", startOptions);
                expectation++;
            }
            break;
        case 1:
            if (response == 0) {

                expectation++;
            }
    }

    //sessionBus.disconnect("", sessionReply.value().path(), "org.freedesktop.portal.Request", "Response", this, SLOT(handleResponse(uint,QVariantMap)));

    /*QVariantMap notifyOptions;
    qInfo() << interface->call("NotifyKeyboardKeycode", *sessionHandle, notifyOptions, 38, 1u);
    qInfo() << interface->call("NotifyKeyboardKeycode", *sessionHandle, notifyOptions, 38, 0u);*/
}



void KeyboardSimulator::writeChar(const QString &character) {
    qInfo() << character;
};
