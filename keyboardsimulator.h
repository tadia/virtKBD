#ifndef KEYBOARDSIMULATOR_H
#define KEYBOARDSIMULATOR_H

#include <QtQml/qqmlregistration.h>
#include <QObject>
#include <QDBusInterface>
#include <QDBusConnection>
#include <QDBusReply>

class KeyboardSimulator : public QObject
{
    Q_OBJECT
    QML_ELEMENT

private:
    QDBusInterface *interface;
    QDBusConnection sessionBus = QDBusConnection::sessionBus();
    QDBusReply<QDBusObjectPath> sessionReply;
    int expectation = 0;

private slots:
    void handleResponse(uint response, QVariantMap data);

public:
    explicit KeyboardSimulator(QObject *parent = nullptr);
    ~KeyboardSimulator();
    Q_INVOKABLE void writeChar(const QString &character);
    Q_INVOKABLE void makeRequest();

signals:
};

#endif // KEYBOARDSIMULATOR_H
